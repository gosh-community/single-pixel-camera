## Purpose and Background

The purpose of this project is to make a fully open source, low cost, accessble single pixel camera.  The applications for such a device include hyperspectral imaging, creating images for wifi or other short wave signals, infra-red (heat) imaging, or other applications.  

The idea for the project came from Richard Bowman at the 2017 Gathering for Open Science Hardware (GOSH) and is a GOSH collaboration.

## Discussion

The discussion group for this project is on the GOSH Forum here:
https://forum.openhardware.science/c/communities/single-pixel-camera-dev-group

## Supporters

Besides the people contributing to this repository and discussions, material contributions for this project came from Hamamatsu Photonics (http://www.hamamatsu.com), GroupGets (https://groupgets.com/), and Our Sci (www.our-sci.net).  Thank you so much for your support!

<a href="http://www.hamamatsu.com/"><img src="/hamamatsu_logo.png" width="200   "></a>
  
<a href="http://groupgets.com/"><img src="/gg-logo-l.png" width="200"></a>

<a href="http://our-sci.net/"><img src="/our-sci-logo-long-64.png" width="200"></a>

