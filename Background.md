# Background


The single-pixel camera is a way of forming images (i.e. measuring the amount of light that hits each point in some "image plane") without needing a conventional camera sensor.  Cameras work by having one detector for each pixel - and CMOS technology makes it possible to have millions of such detectors for very low cost.  If silicon photodetectors won't work, however, life gets very expensive indeed - it's sometimes possible to use the "camera" approach with other detectors (e.g. single photon photodiodes for very weak light, time-resolved detectors for depth imaging, spectrometers for spectrally resolved images, non-silicon photodetectors for IR light).  However, this is usually pretty expensive, and often the pixelated detectors have poorer sensitivity or higher noise than individual detectors (which are often pretty cheap).

Single-pixel cameras need two key components; a "bucket" detector, that measures the signal of interest (called intensity for simplicity's sake), and a mask in the image plane that blocks out some parts of the image.  They form an image by measuring the intensity for lots of different masks - for each mask, this tells us how much intensity from the image gets through the mask.  Because we know the shape of the mask, and the intensity "overlap" between the mask and the image (i.e. how well the two shapes match - or how much intensity from the image fits through the gaps in the mask), we can do some maths (a matrix inversion, which may or may not be underconstrained and require regularisation depending on how many masks we used and now many pixels we're trying to reconstruct) and recover what the image looks like - without needing a pixelated camera.

The single pixel camera is an idea that's been around for a while now - it's variously called "single pixel camera", "compressive imaging", "computational ghost imaging" and no doubt many other names.  It's extensively written about in the scientific literature, and there should be a list of references at the bottom of this article, including some very good tutorial reviews.  It's important to realise that the single-pixel camera is rarely better than a silicon camera, if what you want to do is take a visible-light image.

It is worth clearing up a few common misconceptions about this - as there's a lot of hogwash floating around the literature.
* Generally, you still need imaging optics - *this is not a lensless camera*.  There are lots of misleading claims about this - some true, some less so.  But ultimately, you do still end up making an optical image somewhere.
* It's often helpful to think in terms of "retrodiction", meaning reversing the light rays.  If you replace your "bucket" detector with a lamp, and your mask with a camera sensor, generally you'd get the same image as with the single-pixel camera.  Lots of people compare a "single pixel camera" to a regular camera by replacing the bucket detector with a camera - this is often not a very good comparison, and can lead to you thinking the single pixel camera is doing something exciting - when in fact it is not.  If there's some fundamental difference between the illumination light and the light that's collected, the retrodiction analogy breaks down.  However, it is often still a helpful starting point.

Here's a really good video showing several existing technologies for making a hyperspectral camera.
https://www.youtube.com/watch?v=sVTuh6h0O1k#action=share

## Available firmware

* https://impfs.github.io/review/ - nice bit of code and review.
* https://github.com/groupgets/c12880ma - original groupgets code.  I'm told this could be improved (made faster) with some effort

## Ways to reduce the number of masks

* MIT recently published a paper reducing the number of masks by a factor of 50 to get equivalent image quality - http://www.photoxels.com/mit-speeds-up-the-single-pixel-camera/ .  Downside - this requires femtosecond response timing, so probably not that useful but worth reading.
* Here's another example of a clever idea, taking advanting of the slight differences between absorbance of two different camera types:  http://www.cse.yorku.ca/~mbrown/pdf/cvpr_2016_hyperspectral.pdf

## Recent related projects and literature

* A project making to build a device to do Selective Plane Illumination Microscopy (SPIM) http://openspim.org/Welcome_to_the_OpenSPIM_Wiki.  Also uses a Hamamatsu camera.
* 
## Recent literature on the subject
* [ Real-time imaging of methane gas leaks using a single-pixel camera](https://www.osapublishing.org/DirectPDFAccess/797576E0-AD67-4EDF-ADABA31B6124200A_359510/oe-25-4-2998.pdf?da=1&id=359510&seq=0&mobile=no)
* [Lensless Imaging With Compressive Ultrafast Sensing](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7882664)
A method for lensless imaging based on compressive ultrafast sensing
* [Compressive Ultrafast Single Pixel Camera](https://pdfs.semanticscholar.org/e927/407633a6599455fc95f2f29bd3e5409ba74a.pdf?_ga=2.124960522.1039200486.1534777712-388177229.1534362864)
* [A single-pixel hyperspectral imaging system for Cancer Margin Detection](https://search.proquest.com/docview/2040868676?pq-origsite=gscholar)

A completely different approached to affordable hyperspec: exploit slight differences in normal camera sensors and combine a bunch of them.  See paper [Do It Yourself Hyperspectral Imaging with Everyday Digital Cameras](http://www.cse.yorku.ca/~mbrown/pdf/cvpr_2016_hyperspectral.pdf)