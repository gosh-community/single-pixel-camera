# Comments on the first version of the spinning-mask idea

I think this is potentially a really nice way to produce a single pixel imager.  The most common way is to use a DLP chip (essentially a hacked micro projector, though most folk use the TI development board for convenience).  This has impressive specs: patterns display at up to 1.4kHz or even 20kHz (depending on budget), timing is really good, and the masks can be arbitrary.  However, it costs from £400 up to £20k, which is a bit steep for many applications!

Using a spinning disk has, to my mind, advantages and disadvantages.  Good news first:
* Spinning a disk quickly is easy - can potentially get a very high pattern rate.
* Can be pretty cheap.
* Patterns get translated across the image - might be able to use this to improve resolution

Disadvantages:
* Patterns translate across the image - motion blur might be a problem for long exposures, or you'd have to stop the disk at each position (tricky to go fast).
* You'd need to encode the position of the disk very precisely, and account for any wobble in the rotation axis.  I think that could be done though.

I am also not sure if simply rotating a mask about its centre would be able to give (enough) independent patterns; it constrains you to have some symmetry between all the patterns, which could be a problem.  That can be simulated, though.  I'd always assumed we'd use a strip near the edge - which also simplifies the optical design.

Lastly, bear in mind that the optical design needs to have an imaging system between the object and the mask - the one that's shown in the PDF would work, but only as a pinhole camera - the efficiency would be frighteningly low.  Using a lens in front of the mask to image the scene onto the mask should work much better...

# Response

That answers some of my key go / no go questions - is the basic concept feasible (yes, plus caveats).  My responses;

* Is the math of a spinning disc with non-square 'holes' significantly harder than square holes?  Does that require some more work to be done to make the math work?
* I'll update the optics as a requirement - we need something more robust than a pinhole camera for sure.  Any suggestions on optics or places to look for them (or selection process) would be super helpful, I'm no expect on traditional optics of that type.
* I'll update the description page to include blur evaluation as a testing requirement and potential design flaw, and 'wobble-free', all of which would need to be addressed.  This depends on the characteristics of the motor and mechanical design, so there will be some back and forth on the 'we need something to do this', 'well I found a motor that does this', 'well maybe that's ok given that', kind of stuff.  
* it is possible to use two spinning disks (Andriy Herts suggested Nipkow disks for example, though there are many possible designs) to get a wider range of mask types.  This is tricker to design, and maybe harder to do the math on and make more wobble free.  But if you think this would help significantly, it's a possible design avenue.

If you are familiar with inkscape, could you add the location and any info about the optical design into the inkscape file and re-save as PDF?  I could try but you are more of the expert there.
Also, you suggested we could simulate some of these ideas.  I think having a simulation we can run easily would really speed up our initial development so instead of guessing about how much one design is more efficient/faster/better than another, we'll just know... how hard would it be to set up such a simulation?

