# Concept: Spinning disk as mask

The idea here is to use a spinning disc with randomized holes in it in order to create the mask to produce enough pictures to generate an image.  The advantage is it's lower cost than using a DLP (an array of tiny mirros) and it can be laser cut or 3D printed.

## Servo requirements to spin the mask:
* requires 360 full degree motion
* good resultion (equal number of positions as the number of divisions in the mask wheel)
* good speed (at least as fast to move as the minimum read time by the mini spec (about 8 ms see below))
* ideally we need as many possible positions equal to the number of required masks... maybe 2000 - 3000 based on previous research?

## Mechanical requirements:
* Wobble could introduce noise and reduce quality or increase time to take image.  'wobble-free' designs are preferred
* Some kind of optic is required on the front of the design to improve light capture

## Time per single pixel data collection: How long it takes to collect data form the Hamamtsu mini spec:

Typical read times for 256 channels between 350us to 740nm (or 900nm depending on model)...

... depends on the integration time, so for decent lighting
--> 8000 us (8ms) to read 256 channels (from 350 - 740nm)

... for low lighting
--> 256000 us (256us) to read 256 channels

Plus it takes an additional ~2.8ms to save the data (8us at delayTime == 1us, 256 channels, 3us analog read time if we use a faster ADC (1*8+3)*256 = 2816us = 2.8ms

So let's assume 8ms + 2.8ms = 10.2ms to read all channels on the spec.

And let's assume we need 2000 unique masks (one full rotation of the mask creates 2000 unique positions).  

2000 positions * 10.2ms = 20200ms = 20.2 seconds (yikes!).  

Also, the positions are not fully random, so they will likely be less efficient than a fully randomized position set.  So 2000 positions may produce not the best image.  

**So that image speed is pretty low... given that, what would be acceptable applications?**
- useful for measuring your arm or an apple, where you could hold it steady for that period of time.
- not so useful for measuring a field with lots of wind blowing aroun the leaves :)

**Ways to lower the time**

It's possible we can lower the integration time using a better ADC, but there will be a limit on that.  It's also possible for close up samples to shine a light on the sample to increase the signal response (allowing you to effectively decrease the integration time).
I don't think the time-of-flight method probably won't work here that'd be a totally different setup-->
https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiVy-SS8rPTAhWo3YMKHdgfBUgQFggjMAA&url=http%3A%2F%2Fnews.mit.edu%2F2017%2Ffaster-single-pixel-camera-lensless-imaging-0330&usg=AFQjCNFIdTeeKZlZD1yw5c5IUxtF2YD0XA&sig2=N8QRRmg690cKbVHyW9398g
Are there other ways to decrease the number of pictures to generate an image?

# COMMENTS
_make changes directly to the above document, but add comments or suggestions that you're not confident about here_

## Richard's comments
I think this is potentially a really nice way to produce a single pixel imager.  The most common way is to use a DLP chip (essentially a hacked micro projector, though most folk use the TI development board for convenience).  This has impressive specs: patterns display at up to 1.4kHz or even 20kHz (depending on budget), timing is really good, and the masks can be arbitrary.  However, it costs from £400 up to £20k, which is a bit steep for many applications!

Using a spinning disk has, to my mind, advantages and disadvantages.  Good news first:
* Spinning a disk quickly is easy - can potentially get a very high pattern rate.
* Can be pretty cheap.
* Patterns get translated across the image - might be able to use this to improve resolution

Disadvantages:
* Patterns translate across the image - motion blur might be a problem for long exposures, or you'd have to stop the disk at each position (tricky to go fast).
* You'd need to encode the position of the disk very precisely, and account for any wobble in the rotation axis.  I think that could be done though.

I am also not sure if simply rotating a mask about its centre would be able to give (enough) independent patterns; it constrains you to have some symmetry between all the patterns, which could be a problem.  That can be simulated, though.  I'd always assumed we'd use a strip near the edge - which also simplifies the optical design.

Lastly, bear in mind that the optical design needs to have an imaging system between the object and the mask - the one that's shown in the PDF would work, but only as a pinhole camera - the efficiency would be frighteningly low.  Using a lens in front of the mask to image the scene onto the mask should work much better...
